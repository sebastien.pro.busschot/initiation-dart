class MatterModel {
  String _name;
  List<double> _notes;

  MatterModel(name) {
    set_Name(name);
    set_Notes([]);
  }

  String get_Name() {
    return _name;
  }

  void set_Name(String name) {
    _name = name;
  }

  List<double> get_Notes() {
    return _notes;
  }

  void set_Notes(List<double> notes) {
    _notes = notes;
  }

  void addNote(double el) {
    if (el >= 0 && el <= 20) {
      _notes.add(el);
    } else {
      print(
          "Impossible d'ajouter $el, une note doit être situé en tre 0 et 20");
    }
  }
}
