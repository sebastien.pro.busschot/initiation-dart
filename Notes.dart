import 'MatterModel.dart';

class Notes {
  List<MatterModel> _matterList;

  Notes() {
    _matterList = [];
  }

  bool addNote(String matter, double note) {
    for (int i = 0; i < _matterList.length; i++) {
      if (_matterList[i].get_Name() == matter) {
        _matterList[i].addNote(note);
      }
    }
    return false;
  }

  bool addMatter(String matter) {
    for (int i = 0; i < _matterList.length; i++) {
      if (_matterList[i].get_Name() == matter) {
        return false;
      }
    }
    _matterList.add(MatterModel(matter));
    return true;
  }

  List<double> getNotesByMatter(matter) {
    for (MatterModel matterM in _matterList) {
      if (matterM.get_Name() == matter) {
        return matterM.get_Notes();
      }
    }
    return null;
  }

  bool printMatters() {
    if (_matterList.length == 0) {
      print("Il n'y à pas de matières enregistrées.");
      return false;
    }

    String printed = "";
    for (int i = 0; i < _matterList.length; i++) {
      int menu = i + 1;
      printed += "$menu) " + _matterList[i].get_Name() + " ";
    }
    print(printed);
    return true;
  }

  List<MatterModel> getMatters() {
    return _matterList;
  }

  double matterAverage(int matter) {
    List<double> notes = _matterList[matter].get_Notes();
    if (matter >= 0 && matter < notes.length) {
      double sum = 0;
      for (double note in notes) {
        sum += note;
      }
      return sum / notes.length;
    }
    return null;
  }

  double globalAverage() {
    double sum = 0;
    int lenght = _matterList.length;
    for (int i = 0; i < _matterList.length; i++) {
      double av = matterAverage(i);
      if (av == null) {
        lenght--;
      } else {
        sum += matterAverage(i);
      }
    }

    return lenght == 0 ? null : sum / lenght;
  }
}
