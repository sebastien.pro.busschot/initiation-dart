import 'MatterModel.dart';
import 'Notes.dart';
import 'NE_PAS_TOUCHER/user_input.dart' as input;

class Menu {
  Notes _noteSystem;

  Menu() {
    this._noteSystem = Notes();
  }

  void run() {
    const String message =
        "Menu principal :\n1) Ajouter un note\n2) Ajouter une matière\n3) Récupérer les notes d'une matière\n4) Récupérer toutes les notes\n5) Récupérer la moyenne d'une matière\n6) Récupérer la moyenne globale\n7) Quitter";

    while (true) {
      String select = input.readText(message);
      switch (select) {
        case "1":
          addNote();
          break;
        case "2":
          addMatter();
          break;
        case "3":
          getNotesByMatter();
          break;
        case "4":
          getNotes();
          break;
        case "5":
          getAverageByMatter();
          break;
        case "6":
          getGlobalAverage();
          break;
        case "7":
          print("Au revoir !");
          return;
          break;
        default:
          print("Menu : choix de 1 à 7");
      }
    }
  }

  void addNote() {
    bool mattersNotNull = _noteSystem.printMatters();

    if (mattersNotNull) {
      int indexMatter =
          input.readInt("A quelle matière voulez-vous ajouter une note?") - 1;
      int max = _noteSystem.getMatters().length;
      if (indexMatter >= 0 && indexMatter < max) {
        double note = input.readDouble("Quelle note?");
        _noteSystem.getMatters()[indexMatter].addNote(note);
      } else {
        print("choisissez un nombre compris entre 1 et $max");
      }
    }
  }

  void addMatter() {
    String newMatter =
        input.readText("Quelle est le nom de la nouvelle matière?");

    _noteSystem.addMatter(newMatter);
  }

  void getNotes() {
    for (MatterModel matter in _noteSystem.getMatters()) {
      print(matter.get_Name() + "=>" + matter.get_Notes().toString());
    }
  }

  void getNotesByMatter() {
    bool exitMatters = _noteSystem.printMatters();

    if (exitMatters) {
      int indexMatter =
          input.readInt("De quelle matière voulez-vous voir les notes?") - 1;
      int max = _noteSystem.getMatters().length;
      if (indexMatter >= 0 && indexMatter < max) {
        print(_noteSystem.getMatters()[indexMatter].get_Notes());
      } else {
        print("choisissez un nombre compris entre 1 et $max");
      }
    }
  }

  void getAverageByMatter() {
    bool exitMatters = _noteSystem.printMatters();

    if (exitMatters) {
      int indexMatter =
          input.readInt("De quelle matière voulez-vous voir la moyenne?") - 1;
      int max = _noteSystem.getMatters().length;
      if (indexMatter >= 0 && indexMatter < max) {
        double av = _noteSystem.matterAverage(indexMatter);
        if (av == null) {
          print("Pas de moyenne");
        } else {
          print("Moyenne $av");
        }
      } else {
        print("choisissez un nombre compris entre 1 et $max");
      }
    }
  }

  void getGlobalAverage() {
    bool exitMatters = _noteSystem.printMatters();

    if (exitMatters) {
      double av = _noteSystem.globalAverage();
      if (av == null) {
        print("Pas de moyenne");
      } else {
        print("Moyenne globale à $av");
      }
    }
  }
}
